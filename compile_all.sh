#!/bin/bash

for i in ./* ; do
  if [ -d "$i" ]; then
    cd $i/bin
    ./clean
    ./make ; ./make win
    cd ../..
  fi
done
