#ifndef GAME_H
#define GAME_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_layer.h>
#include "../include/rotozoom.h"

#define RED 0xFF0000
#define GREEN 0x00FF00
#define BLUE 0x0000FF
#define BACKCOLOR 0x0F0F0F

#define FACTOR 0.1

enum SnakeDirections
{
  D_UP, D_DOWN, D_LEFT, D_RIGHT
};

enum Putaditas
{
  P_ZOOM, P_CONTROLS, P_ROTO, P_FOO
};

struct Snake
{
  Vector2<int> position;
  int tam = 3;
  int rango = 10;
  int dir = D_RIGHT;
};

class Game : public RF_Layer
{
    public:
        Game():RF_Layer("Game"){}
        virtual ~Game(){}

        virtual void Start();
        virtual void Update();
        virtual void Draw();

    private:
      void KeyUpdate();
      void InputUpdate();
      void SnakeUpdate();
      void GridUpdate();

      void Eaten();
      void GetFood();

      void MegaFoodUpdate();
      void EatenMegaFood(bool eaten = true);
      void GetMegaFood();

      void PutaditaUpdate();
      void GetPutadita();

      void DeadState();

      Rotozoom<int> *grid;
      Snake snake;
      float deltaCount = 0.0, gameSpeed;

      int tmp, color;
      int i,j;
      int key = -1;
      bool pressed = false;

      string score;
      Vector2<int> scorePosition = {20,20};

      Vector2<float> shake = {0.0,0.0};
      Vector2<float> shaking;

      float megaFoodTime = 0.0;
      Vector2<int> megafood;

      int putadita = P_FOO;
      float putaditaTime = 0.0;

      bool putaditas[P_FOO];
      int putCont = 0;
      void ClearPutaditas();
};

#endif //GAME_H
