#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include <RosquilleraReforged/rf_process.h>

class MainProcess : public RF_Process
{
  public:
    MainProcess():RF_Process("MainProcess"){}
    virtual ~MainProcess(){}

    virtual void Start();
    virtual void Update();

    int& state(){return stateMachine;}

  private:
    int stateMachine = 0;
    RF_Process* scene = nullptr;
};

#endif //MAINPROCESS_H
