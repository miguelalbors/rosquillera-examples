#ifndef ROTOZOOM_H
#define ROTOZOOM_H

#include <RosquilleraReforged/rf_structs.h>
#include <vector>
#include <cmath>
using namespace std;
using namespace RF_Structs;

template <typename T>
class Rotozoom
{
  public:
    Rotozoom(Vector2<int> size)
    {
      setSize(size.x, size.y);
      setOffset(width >> 1, height >>1);
    }
    Rotozoom(int w, int h)
    {
      setSize(w,h);
      setOffset(width >> 1, height >>1);
    }
    Rotozoom(int w, int h, T *rawdata)
    {
      setSize(w,h);
      setOffset(width >> 1, height >>1);
      copyRawData(rawdata);
    }

    virtual ~Rotozoom(){}

    void setOffset(Vector2<int> offSet)
    {
      setOffset(offSet.x, offSet.y);
    }
    void setOffset(int x, int y)
    {
      offset.x = (x >= 0) ? x : 0;
      offset.y = (y >= 0) ? y : 0;
    }
    Vector2<int> getOffset()
    {
      return offset;
    }

    void setSize(Vector2<int> size)
    {
      setSize(size.x, size.y);
    }
    void setSize(int w, int h)
    {
      width = (w > 0) ? w : 1;
      height = (h > 0) ? h : 1;
    }
    Vector2<int> getSize()
    {
      return {width,height};//Vector2<int>(width,height);
    }

    int Width(){return width;}
    int Height(){return height;}

    void setFactor(Vector2<float> f)
    {
      setFactor(f.x, f.y);
    }
    void setFactor(float f)
    {
      factor.x = f;
      factor.y = f;
    }
    void setFactor(float x, float y)
    {
      factor.x = x;
      factor.y = y;
    }
    Vector2<float> getFactor()
    {
      return factor;
    }

    void setRotation(float r)
    {
      rotation = r;
    }
    float getRotation()
    {
      return rotation;
    }

    void copyRawData(T *rawdata)
    {
      data.clear();
      for(int i = 0; i < width*height; i++)
      {
        data.push_back(rawdata[i]);
      }
    }
    void addData(T rawdata)
    {
      data.push_back(rawdata);
    }

    T get(Vector2<int> pos)
    {
      return get(pos.x, pos.y);
    }
    T get(int x, int y)
    {
      return data[y*width + x];
    }
    void set(Vector2<int> pos, T value)
    {
      set(pos.x, pos.y, value);
    }
    void set(int x, int y, T value)
    {
      data[y*width + x] = value;
    }

    T getRZ(int x, int y)
    {
      tmp = getCoords(x, y);
      return data[tmp.y*width + tmp.x];
    }
    void setRZ(int x, int y, T value)
    {
      tmp = getCoords(x,y);
      data[tmp.y*width + tmp.x] = value;
    }

    Vector2<int> getCoords(Vector2<int> position)
    {
      return getCoords(position.x, position.y);
    }
    Vector2<int> getCoords(int x, int y)
    {
      tmp.x = (x + offset.x) * factor.x;
      tmp.y = (y + offset.y) * factor.y;

      retorno.x = tmp.x*cos(rotation) - tmp.y*sin(rotation);
      retorno.y = tmp.x*sin(rotation) + tmp.y*cos(rotation);

      if(retorno.x < 0 || retorno.x >= width)
      {
        retorno.x = retorno.x % width;
        if(retorno.x < 0){retorno.x = width + retorno.x;}
      }

      if(retorno.y < 0 || retorno.y >= height)
      {
        retorno.y = retorno.y % height;
        if(retorno.y < 0){retorno.y = height + retorno.y;}
      }

      return retorno;
    }

    void clear()
    {
      data.clear();
    }

  private:
    int width = 0, height = 0;
    vector<T> data;
    Vector2<float> factor = {1.0, 1.0};
    Vector2<int> offset;
    float rotation = 0.0;

    Vector2<int> tmp, retorno;
};
/*

  000000010000000000000000000000

  0 0 0 0 0
  0 0 1 0 0
  0 0 0 0 0
  0 0 0 0 0
  0 0 0 0 0
  0 0 0 0 0

2



*/
#endif //ROTOZOOM_H
