#ifndef GAME_H
#define GAME_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_layer.h>
#include "../include/rotozoom.h"

#define RED 0xFF0000
#define GREEN 0x00FF00
#define PINK 0xFF00FF
#define BROWN 0xFF8200

#define BLUE 0x0000FF
#define DARKBLUE 0x000066
#define WHITE 0xFFFFFF
#define WHITE2 0x666666
#define YELLOW 0xFFFF00
#define BACKCOLOR 0x0F0F0F

#define FACTOR_X 0.0225
#define FACTOR_Y 0.035

#define FILAS_MAPA         25
#define COLUMNAS_MAPA      29

enum MapElements
{
  me_door = -3,
  me_wall = -4,
  me_megafood = -2,
  me_food = -1,
  me_space = 0,
};

enum SnakeDirections
{
  D_UP, D_DOWN, D_LEFT, D_RIGHT, D_FOO
};

enum Putaditas
{
  P_ZOOM, P_CONTROLS, P_ROTO, P_FOO
};

struct Snake
{
  Vector2<int> position;
  int tam = 2;
  int dir = D_RIGHT;
};

struct Phantom
{
  int id;
  Vector2<int> position;
  Vector2<int> oldposition;
  int dir;
  int tile = 0;
};

/*struct Phantom
{
  Vector2<int> position;
  int tile_value;
};*/

class Game : public RF_Layer
{
    public:
        Game():RF_Layer("Game"){}
        virtual ~Game(){}

        virtual void Start();
        virtual void Update();
        virtual void Draw();

    private:
      void newgame();
      void levelup();

      void KeyUpdate();
      void InputUpdate();
      void SnakeUpdate();
      void GridUpdate();

      void Eaten();
      void EatenMegaFood();

      void PhantomUpdate();
      void RandPhantom(Phantom *ph);
      void MovePhantom(Phantom *ph);

      void DeadState();

      Rotozoom<int> *grid;
      Snake snake;
      Vector2<int> snake_position;
      float deltaCount = 0.0, gameSpeed;
      int foo_cont;

      int level = 0;

      int tmp, color;
      int i,j;
      int key = -1;
      bool pressed = false;

      string score;
      int score_val = 0;
      Vector2<int> scorePosition = {20,20};

      Vector2<float> shake = {0.0,0.0};
      Vector2<float> shaking;

      Phantom phantoms[4];

      string mapa[FILAS_MAPA] = {
        "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "x@............x............@x",
        "x.xxxx.xxxxxx.x.xxxxxx.xxxx.x",
        "x.x  x.x    x.x.x    x.x  x.x",
        "x.xxxx.xxxxxx.x.xxxxxx.xxxx.x",
        "x.....@...............@.....x",
        "x.xxxx.x.xxxxxxxxxxx.x.xxxx.x",
        "x......x......x......x......x",
        "xxxxxx.xxxxxx.x.xxxxxx.xxxxxx",
        "     x.x......i......x.x     ",
        "     x.x.xxxxxvxxxxx.x.x     ",
        "xxxxxx.x.xf       fx.x.xxxxxx",
        ".........x         x.........",
        "xxxxxx.x.xf       fx.x.xxxxxx",
        "     x.x.xxxxxvxxxxx.x.x     ",
        "     x.x.............x.x     ",
        "xxxxxx.x.xxxxxxxxxxx.x.xxxxxx",
        "x............@x@............x",
        "x.xxxx.xxxxxx.x.xxxxxx.xxxx.x",
        "x....x.................x....x",
        "xxxx.x.x.xxxxxxxxxxx.x.x.xxxx",
        "x......x......x......x......x",
        "x.xxxxxxxxxxx.x.xxxxxxxxxxx.x",
        "x@.........................@x",
        "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
      };
};

#endif //GAME_H
