# Pakumanu
Pakumanu es la perfecta fusión entre el snake y el pacman.

## Dependencias
La lista de dependencias es la siguiente:
 * cmake >= 2.8.0
 * c++17
 * La rosquillera framework - Reforged (https://gitlab.com/yawin/RosquilleraReforged)

## Compilación
Estando en la carpeta del proyecto:

     cd ./bin
     cmake ..
     make all

## Ejecución
El ejecutable se generará en `./bin`. En esa carpeta también hay una carpeta llamada `res` que es imperativo que esté en el mismo directorio que el ejecutable.

## Cómo jugar
El objetivo en cada nivel es limpiar todas las casillas blancas y grises. ¡Cuidado! Las casillas grises aumentarán la longitud de la serpiente. Que no se choque contra sí misma.

 * La tecla `ENTER` inicia la partida.
 * Las teclas `WASD` son para que nuestro snakman se desplace.
 * La tecla `ESC` cierra el juego.
