#include "../include/game.h"
#include "../include/mainprocess.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>

void Game::Start()
{
    RF_Layer::Start();
    RF_Primitive::clearSurface(graph, 0xF0F0F0);
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    grid = new Rotozoom<int>(COLUMNAS_MAPA,FILAS_MAPA);
    newgame();
}

void Game::levelup()
{
  shake.x = level*0.5;
  shake.y = level*0.5;
  level++;
}

void Game::newgame()
{
  levelup();

  grid->setFactor(FACTOR_X, FACTOR_Y);
  grid->setOffset(0.0,0.0);
  grid->clear();

  int ph = 0;
  char t; int v;
  for(j = 0; j < grid->Height(); j++)
  {
    for(i = 0; i < grid->Width(); i++)
    {
      v = 0;
      t = mapa[j].c_str()[i];

      switch(t)
      {
        case 'f':
          v = -5 - ph;
          phantoms[ph].id = v;
          phantoms[ph].position = {i,j};
          ph++;
          break;
        case 'x':
          v = me_wall;
          break;
        case 'v':
          v = me_door;
          break;
        case '.':
          v = me_food;
          break;
        case '@':
          v = me_megafood;
          break;
        case 'i':
          snake.position = {i,j};
          snake.tam = 1 + level;
          snake.dir = D_RIGHT;
          break;
        default:
          v = me_space;
          break;
      }
      grid->addData(v);
    }
  }

  gameSpeed = 0.1;
}

void Game::Update()
{
  if(score_val >= 290)
  {
      newgame();
      score_val = 0;
  }

  if(snake.tam > 0)
  {
    KeyUpdate();

    deltaCount += RF_Engine::instance->Clock.deltaTime;
    if(deltaCount >= gameSpeed)
    {
      deltaCount = 0.0;

      InputUpdate();
      SnakeUpdate();
      GridUpdate();
      PhantomUpdate();
    }
  }
  else
  {
    DeadState();
  }
}

void Game::KeyUpdate()
{
  if(RF_Input::key[_up] || RF_Input::key[_w]){key = _up;}
  else if(RF_Input::key[_down] || RF_Input::key[_s]){key = _down;}
  else if(RF_Input::key[_left] || RF_Input::key[_a]){key = _left;}
  else if(RF_Input::key[_right] || RF_Input::key[_d]){key = _right;}
}

void Game::InputUpdate()
{
  if(key == _up && snake.dir != D_DOWN && grid->get(snake.position.x, snake.position.y-1) > -3){snake.dir = D_UP; foo_cont = snake.tam;}
  else if(key == _down && snake.dir != D_UP && grid->get(snake.position.x, snake.position.y+1) > -3){snake.dir = D_DOWN; foo_cont = snake.tam;}
  else if(key == _left && snake.dir != D_RIGHT && grid->get(snake.position.x-1, snake.position.y) > -3){snake.dir = D_LEFT; foo_cont = snake.tam;}
  else if(key == _right && snake.dir != D_LEFT && grid->get(snake.position.x+1, snake.position.y) > -3){snake.dir = D_RIGHT; foo_cont = snake.tam;}

  key = -1;
}
void Game::SnakeUpdate()
{
  snake_position = snake.position;

  switch(snake.dir)
  {
    case D_UP:
      snake.position.y--;
      break;
    case D_DOWN:
      snake.position.y++;
      break;
    case D_LEFT:
      snake.position.x--;
      break;
    case D_RIGHT:
      snake.position.x++;
      break;
  }

  if(snake.position.x < 0){snake.position.x = grid->Width()-1;}
  else if(snake.position.x >= grid->Width()){snake.position.x = 0;}

  if(snake.position.y < 0){snake.position.y = grid->Height()-1;}
  else if(snake.position.y >= grid->Height()){snake.position.y = 0;}

  tmp = grid->get(snake.position);

  if((tmp > 0 && snake.dir != D_FOO) || tmp <= -5)
  {
    RF_TextManager::Write("Has perdido", {255,255,255}, {RF_Engine::MainWindow()->width()>>1, RF_Engine::MainWindow()->height()>>1}, id);
    RF_TextManager::Write("Pulsa ENTER", {255,255,255}, {RF_Engine::MainWindow()->width()>>1, 65 + RF_Engine::MainWindow()->height()>>1}, id);
    snake.tam = -1;
    return;
  }
  else if(tmp == -1)
  {
    Eaten();
  }
  else if(tmp == -2)
  {
    EatenMegaFood();
  }
  else if(tmp <= -3)
  {
    snake.position = snake_position;
    if(foo_cont <= 1)
    {
      snake.dir = D_FOO;
    }
    else
    {
      foo_cont--;
    }
  }

  grid->set(snake.position, snake.tam);
}
void Game::GridUpdate()
{
  for(j = 0; j < grid->Height(); j++)
  {
    for(i = 0; i < grid->Width(); i++)
    {
      if(grid->get(i,j) > 0)
      {
        grid->set(i,j,grid->get(i,j)-1);
      }
    }
  }

  shaking.x = (float)grid->getOffset().x + shake.x * sin(RF_Engine::instance->Clock.currentTime*0.001);
  if(shaking.x < 0){ shaking.x = graph->w + shaking.x;}
  shaking.y = (float)grid->getOffset().y + shake.y * cos(RF_Engine::instance->Clock.currentTime*0.001);
  if(shaking.y < 0){ shaking.y = graph->h + shaking.y;}

  grid->setOffset((int)shaking.x, (int)shaking.y);
}

void Game::Eaten()
{
  score_val++;
}

void Game::EatenMegaFood()
{
  score_val++;
  snake.tam++;
}

void Game::PhantomUpdate()
{
  /*RandPhantom(&phantoms[0]);
  MovePhantom(&phantoms[0]);*/
  for(i = 0; i < 4; i++)
  {
    RandPhantom(&phantoms[i]);
    MovePhantom(&phantoms[i]);
  }
}

void Game::RandPhantom(Phantom *ph)
{
  ph->dir = rand()%D_FOO;
}

void Game::MovePhantom(Phantom *ph)
{
  grid->set(ph->position, ph->tile);

  ph->oldposition = ph->position;
  switch(ph->dir)
  {
    case D_UP:
      ph->position.y--;
      break;
    case D_DOWN:
      ph->position.y++;
      break;
    case D_LEFT:
      ph->position.x--;
      break;
    case D_RIGHT:
      ph->position.x++;
      break;
  }

  if(ph->position.x < 0){ph->position.x = grid->Width()-1;}
  else if(ph->position.x >= grid->Width()){ph->position.x = 0;}

  if(ph->position.y < 0){ph->position.y = grid->Height()-1;}
  else if(ph->position.y >= grid->Height()){ph->position.y = 0;}

  tmp = grid->get(ph->position);


  if(tmp > 0)
  {
    RF_TextManager::Write("Has perdido", {255,255,255}, {RF_Engine::MainWindow()->width()>>1, RF_Engine::MainWindow()->height()>>1}, id);
    RF_TextManager::Write("Pulsa ENTER", {255,255,255}, {RF_Engine::MainWindow()->width()>>1, 65 + RF_Engine::MainWindow()->height()>>1}, id);
    snake.tam = -1;
    return;
  }
  else if(tmp <= -4)
  {
    ph->position = ph->oldposition;
  }

  ph->tile = grid->get(ph->position);
  grid->set(ph->position, ph->id);
}

void Game::Draw()
{
  for(j = 0; j < graph->h; j++)
  {
    for(i = 0; i < graph->w; i++)
    {
      tmp = grid->getRZ(i,j);
      color = BACKCOLOR;
      if(tmp > 0)
      {
        color = YELLOW;
      }
      else if(tmp == -1)
      {
        color = WHITE;
      }
      else if(tmp == -2)
      {
        color = WHITE2;
      }
      else if(tmp == -3)
      {
        color = BLUE;
      }
      else if(tmp == -4)
      {
        color = DARKBLUE;
      }
      else if(tmp == -5)
      {
        color = RED;
      }
      else if(tmp == -6)
      {
        color = GREEN;
      }
      else if(tmp == -7)
      {
        color = PINK;
      }
      else if(tmp == -8)
      {
        color = BROWN;
      }

      RF_Primitive::putPixel(graph, i, j, color);
    }
  }
}

void Game::DeadState()
{
  if(!pressed && RF_Input::key[_return])
  {
    pressed = true;
  }
  else if(pressed && !RF_Input::key[_return])
  {
    RF_Primitive::clearSurface(graph, 0x000000);
    RF_Engine::getTask<MainProcess>(father)->state() = 1;
    signal = RF_Structs::S_SLEEP;
  }
}
