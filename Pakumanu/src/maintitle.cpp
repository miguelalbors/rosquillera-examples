#include "../include/maintitle.h"
#include "../include/mainprocess.h"

#include <RosquilleraReforged/rf_primitive.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>

using namespace RF_Structs;

void Title::Start()
{
  transform.position.x = RF_Engine::MainWindow()->width() >> 1;
  transform.position.y = (RF_Engine::MainWindow()->height() >> 1) - 75;

  graph = RF_AssetManager::Get<RF_Gfx2D>("old", "title");
}

void Maintitle::Start()
{
  RF_Layer::Start();
  RF_Primitive::clearSurface(graph, 0x0F0F0F);
  transform.position.x = RF_Engine::MainWindow()->width() >> 1;
  transform.position.y = RF_Engine::MainWindow()->height() >> 1;

  RF_TextManager::Font = RF_AssetManager::Get<RF_Font>("old", "Times_New_Roman");
  RF_Engine::newTask<Title>(id);
  RF_TextManager::Write("Pulsa ENTER para empezar", {255,255,255}, {RF_Engine::MainWindow()->width()>>1, 200 + RF_Engine::MainWindow()->height()>>1}, id);

  RF_Process *text = RF_Engine::getTask(RF_TextManager::Write("Music: Cool Electro Jingle - by JiltedG", {255,255,255}, {0,0}, id));
  text->transform.scale = {0.5,0.5};
  text->transform.position = {RF_Engine::MainWindow()->width() - 10 - (int)((text->graph->w*text->transform.scale.x)/2), RF_Engine::MainWindow()->height() - 10 - (int)((text->graph->h*text->transform.scale.y)/2)};
}

void Maintitle::Update()
{
  rand();
  if(!pressed && RF_Input::key[_return])
  {
    pressed = true;
  }
  else if(pressed && !RF_Input::key[_return])
  {
    RF_Primitive::clearSurface(graph, 0x000000);
    RF_Engine::getTask<MainProcess>(father)->state() = 2;
    signal = RF_Structs::S_SLEEP;
  }
}
