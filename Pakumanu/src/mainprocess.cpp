#include "../include/mainprocess.h"
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>

#include "../include/splashscreen.h"
#include "../include/maintitle.h"
#include "../include/game.h"

void MainProcess::Start()
{
    RF_Engine::addWindow("Pakumanu", 1280, 720, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);//, SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);

    RF_AssetManager::loadAssetPackage("res/old");
    scene = RF_Engine::getTask(RF_Engine::newTask<SplashScreen>(id));
}

void MainProcess::Update()
{
  //ExeControl
    if(RF_Input::key[RF_Structs::_esc] || RF_Input::key[RF_Structs::_close_window])
    {
      RF_Engine::Status() = false;
    }

    switch(stateMachine)
    {
      case 1: //Maintitle
          if(scene && "Maintitle" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_SoundManager::changeMusic("old", "musica");
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Maintitle>(id));
          }
          break;
        case 2: //Game
          if(scene && "Game" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Game>(id));
          }
          break;
      }
}
