#ifndef SCENE6_H
#define SCENE6_H

#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_textmanager.h>
using namespace RF_Structs;

class BezierLetter : public RF_Process
{
    public:
        BezierLetter():RF_Process("BezierLetter"){}
        virtual ~BezierLetter(){}

        virtual void Update();
        virtual void Dispose();

        void configure(string txt, Vector2<int> pos, int retardo = 0);

    private:
        Vector2<int> position;
        string text = "";
        string textID = "";
        float deltaCount = 0.0f;
        int step = 100;
        int x = -40,y = -60;
};

class Scene6 : public RF_Layer
{
    public:
        Scene6():RF_Layer("Scene6"){}
        virtual ~Scene6(){}

        virtual void Start();
        virtual void Update();

        void bezierText(string text, int y);

    private:
        float deltaCount = 0.0f;
        int step = 0, step2 = 0, cuentatexto = 0;
};

#endif // SCENE6_H
