#ifndef RF_3D_OBJECT_H
#define RF_3D_OBJECT_H

#include <vector>
using namespace std;

template<typename T>
struct Vector3{
    T x,y,z;
    Vector3(T x0=0, T y0=0, T z0=0){x=x0; y=y0; z=z0;}
    Vector3(const Vector3& c){x=c.x; y=c.y; z=c.z;}
};
struct Transform3D{
    Vector3<float> position, rotation, scale;
    Transform3D(Vector3<float> _position = Vector3<float>(0.0f,0.0f,0.0f),
                Vector3<float> _rotation = Vector3<float>(0.0f,0.0f,0.0f),
                Vector3<float> _scale = Vector3<float>(0.0f,0.0f,0.0f)      )
                {position = _position; rotation = _rotation; scale = _scale;}
};
struct Faces{
    int a,b,c,d;
    Faces(int a0=0, int b0=0, int c0=0, int d0=-1){a=a0; b=b0; c=c0; d=d0;}
};

class RF_3D_Object
{
    public:
        RF_3D_Object(){};
        virtual ~RF_3D_Object(){};

        Transform3D transform;
        vector<Faces> faces;
        vector<Vector3<float>> vertex; //Lista de entrada de los vértices al ser cargados
                                    //Y lista de vértices tras calcular la rotación

        void finalizeCreation(){
            _vertex = vertex;
        }

        void calculateMesh();
        vector<Vector3<float>> _vertex; //Información original del objeto
    private:
};

#endif // RF_3D_OBJECT_H
