#ifndef SCENE3_H
#define SCENE3_H

#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_engine.h>
#include "../include/rf_3d.h"

#include <math.h>
#include <vector>
using namespace std;

class Scene3 : public RF_Layer
{
    public:
        Scene3():RF_Layer("Scene3"){}
        virtual ~Scene3(){}

        virtual void Start();
        virtual void Update();

        void metaballs(int type = 0);

    private:
        Uint32 bgImg[640][480];
        Uint32 bgImg2[640][480];
        int step = 0;

        float deltaCont = 0, ballC = 1;

        float metang;
        Vector3<int> b1, b2, b3;
        float goo[3] = {0.85,0.90,0.92};
        float radio = 0.8;
};

#endif // SCENE3_H
