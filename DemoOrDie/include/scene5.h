#ifndef SCENE5_H
#define SCENE5_H

#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_textmanager.h>

#include <math.h>
using namespace std;
using namespace RF_Structs;

class Scene5;
class puntoVessel : public RF_Process
{
    public:
        puntoVessel():RF_Process("puntoVessel"){}
        virtual ~puntoVessel(){}

        virtual void Start();
        virtual void Update();

    private:
        int vida = 0;
        Scene5* fScene;
};
class aleatLetra : public RF_Process
{
    public:
        aleatLetra():RF_Process("aleatLetra"){}
        virtual ~aleatLetra(){}

        virtual void Start();
        virtual void Update();
        virtual void Dispose();

        void configure(string text, Vector2<int> StartPosition, int steps = 100, int waitStep = 50);

    private:
        string textID = "";
        string txt="";

        Vector2<float> pos;
        int _steps = 100, sC=0;

        Scene5* fScene;
};

class Scene5 : public RF_Layer
{
    public:
        Scene5():RF_Layer("Scene5"){}
        virtual ~Scene5(){}

        virtual void Start();
        virtual void Update();

        int step = 0, textCont = 0;
        float deltaCount = 0;
        void creaTexto(string txt, Vector2<int> p);
};

#endif // SCENE5_H
