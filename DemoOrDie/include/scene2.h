#ifndef SCENE2_H
#define SCENE2_H

#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_textmanager.h>

#include <vector>
using namespace std;
using namespace RF_Structs;

class scrLetra : public RF_Process
{
    public:
        scrLetra():RF_Process("scrLetra"){}
        virtual ~scrLetra(){}

        virtual void Update();
        virtual void Dispose();

        void configure(string text, Vector2<int> StartPosition, float frecuencia);

    private:
        string textID = "";
        string txt="";

        Vector2<float> pos;
        float freq;
        SDL_Color color;
        SDL_Color colores[7] = {{255,255,255}, {255,255,0}, {255,0,255}, {0,255,255}, {255,0,0}, {0,255,0}, {0,0,255} };
};

class Scene2 : public RF_Layer
{
    public:
        Scene2():RF_Layer("Scene2"){}
        virtual ~Scene2();

        virtual void Start();
        virtual void Update();

        bool Starfield(int limit);
        void Scrolltext(string text, int y);

    private:
        vector<Vector2<int>> stars;
        vector<int> stars_speed;

        float deltaCount = 0.0f;
        bool lastFrame = false, cH = false;
        int step=0, step2=0, cuentaobj=0, objtorend = 0, textcont=0;
};

#endif // SCENE2_H
