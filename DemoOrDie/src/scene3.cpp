#include "../include/scene3.h"
#include <RosquilleraReforged/rf_assetmanager.h>

#include "../include/mainprocess.h"

void Scene3::Start()
{
    RF_Engine::Debug(type);

    RF_Layer::Start();
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    SDL_Surface* bgImgS = RF_AssetManager::Get<RF_Gfx2D>("gfx", "euskal");
    for(int i=0; i<640; i++)
    {
        for(int j=0; j<480; j++)
        {
            bgImg[i][j] = RF_Primitive::getPixel(bgImgS,i,j);
        }
    }

    bgImgS = RF_AssetManager::Get<RF_Gfx2D>("gfx", "manos");
    for(int i=0; i<640; i++)
    {
        for(int j=0; j<480; j++)
        {
            bgImg2[i][j] = RF_Primitive::getPixel(bgImgS,i,j);
        }
    }
}
void Scene3::Update(){
    deltaCont += RF_Engine::instance->Clock.deltaTime;
    if(deltaCont > 0.025f)
    {
        if(8000 > RF_Engine::instance->Clock.fixedCTime())
        {
            metaballs(0);
        }
        else
        {
            metaballs(ballC);

            if(0 == step%30)
            {
                ballC++; if(ballC > 2){ballC=1;}
            }

            if(16800 < RF_Engine::instance->Clock.fixedCTime())
            {
              RF_Primitive::clearSurface(graph, 0x000000);
              RF_Engine::getTask<MainProcess>(father)->state() = 4;
              signal = RF_Structs::S_SLEEP;
            }
        }


        step++;
        deltaCont = 0;
    }
}

void Scene3::metaballs(int type){
    metang = M_PI*step*0.075;
    RF_Primitive::clearSurface(graph, 0x000000);

    b1.x=300+cos(metang*1.25)*200; b1.y=300+sin(metang)*200;    b1.z=20;
    b2.x=300+sin(metang*0.9)*200;  b2.y=300+cos(metang)*200;    b2.z=25;
    b3.x=300+sin(metang+100)*200;  b3.y=300+cos(metang+50)*200; b3.z=30;

    for(int i = 0; i < 640; i++)
    {
        for(int j = 0; j < 480; j++)
        {
            if((b1.z/(pow(sqrt(pow(b1.x-i,2)+pow(b1.y-j,2)),goo[0])) + b2.z/(pow(sqrt(pow(b2.x-i,2)+pow(b2.y-j,2)),goo[1])) + b3.z/(pow(sqrt(pow(b3.x-i,2)+pow(b3.y-j,2)),goo[2])))>radio)
            {
                Uint8 r,g,b;
                switch(type)
                {
                    case 0:
                        RF_Primitive::putPixel(graph,i,j,bgImg[i][j]);
                        break;
                    case 1:
                        RF_Primitive::putPixel(graph,i,j,bgImg[i][j]);
                        break;
                    case 2:
                        RF_Primitive::putPixel(graph,i,j,bgImg2[i][j]);
                        break;
                }
            }
            else
            {
                if(0 < type)
                {
                    if(type == 1)
                    {
                        RF_Primitive::putPixel(graph,i,j,bgImg2[i][j]);
                    }
                    else
                    {
                        RF_Primitive::putPixel(graph,i,j,bgImg[i][j]);
                    }
                }
            }
        }
    }
}
