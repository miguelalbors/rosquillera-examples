#include "../include/scene5.h"
#include <RosquilleraReforged/rf_assetmanager.h>

#include "../include/mainprocess.h"
#include <string>
using namespace std;

void Scene5::Start()
{
    RF_Engine::Debug(type);

    RF_Layer::Start();
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    RF_TextManager::Font = RF_AssetManager::Get<RF_Font>("misc", "Times_New_Roman");
}

void Scene5::Update()
{
    if(16800 > RF_Engine::instance->Clock.fixedCTime())
    {
        if(textCont<3)
        {
            RF_Engine::newTask<puntoVessel>(id);
        }

        deltaCount += RF_Engine::instance->Clock.deltaTime;
        if(0.025 < deltaCount)
        {
            RF_Primitive::clearSurface(graph, 0x000000);
            if(79 == step%80)
            {
                switch(textCont)
                {
                    case 0:
                        creaTexto("Roberta Williams", Vector2<int>(320,360));
                        break;
                    case 1:
                        creaTexto("Jade Raymond", Vector2<int>(220,360));
                        break;
                    case 2:
                        creaTexto("Amy Hennig", Vector2<int>(420,360));
                        break;
                    case 3:
                        creaTexto("Lucy Bradshaw", Vector2<int>(300,360));
                        break;
                    case 4:
                        creaTexto("Kim Swift", Vector2<int>(200,360));
                        break;
                    case 5:
                        creaTexto("Siobhan Reddy", Vector2<int>(100,360));
                        break;
                }

                textCont++;
                step=-1;
            }

            step++;
            deltaCount = 0.0f;
        }
    }
    else
    {
      RF_Primitive::clearSurface(graph, 0x000000);
      RF_Engine::getTask<MainProcess>(father)->state() = 6;
      signal = RF_Structs::S_SLEEP_TREE;
    }
}

void Scene5::creaTexto(string txt, Vector2<int> p){
    int posX = 0;

    int emes = 0;
    for(int i = txt.length()-1; i >= 0; i--)
    {
      if(txt.substr(i,1) == "m")
      {
          emes++;
      }
    }

    for(int i = 0; i < txt.length(); i++)
    {
        if(txt.substr(i,1) == " ")
        {
          posX+=5;
        }
        else if(txt.substr(i,1) == "m")
        {
          emes--;
        }

        RF_Engine::getTask<aleatLetra>(RF_Engine::newTask<aleatLetra>(id))->configure(txt.substr(i,1),Vector2<int>(p.x+posX+(3*emes),p.y), 20, 15);
        posX+=20;
    }
}

void puntoVessel::Start()
{
    fScene = RF_Engine::getTask<Scene5>(father);
    vida = RF_Engine::instance->Clock.fixedCTime() + 10000;

    if(RF_Engine::instance->Clock.fixedCTime() + 10000 > 16800)
    {
      vida = 16800;
    }
}
void puntoVessel::Update()
{
    if(RF_Engine::instance->Clock.fixedCTime() < vida && 16800 > RF_Engine::instance->Clock.fixedCTime())
    {
        int x = 320 + sin(M_PI*((vida + RF_Engine::instance->Clock.fixedCTime())*1.2)*0.00016)*300;
        int y = 120 + sin(M_PI*((vida + RF_Engine::instance->Clock.fixedCTime())*1.2)*5*0.0005) * 100;
        RF_Primitive::putPixel(fScene->graph,x,y, 0xffffff);
        RF_Primitive::putPixel(fScene->graph,x,y+1, 0xffffff);
        RF_Primitive::putPixel(fScene->graph,x+1,y, 0xffffff);
        RF_Primitive::putPixel(fScene->graph,x+1,y+1, 0xffffff);
    }
}

void aleatLetra::Start()
{
    fScene = RF_Engine::getTask<Scene5>(father);
}
void aleatLetra::Update()
{
    if("" < textID && RF_Engine::existsTask(textID))
    {
      RF_TextManager::DeleteText(textID);
    }

    Vector2<int> p;
    int st = _steps - fScene->step;

    if(0 >= st)
    {
        if(0 < (_steps + sC) - fScene->step)
        {
            st = 0;
        }
        else if(-(_steps) < (_steps + sC) - fScene->step)
        {
            st = (_steps + sC) - fScene->step;
        }
        else
        {
            signal = S_KILL;
        }
    }

    p.x = (int)pos.x + (rand()%50 - 25) * st;
    p.y = (int)pos.y + (rand()%50 - 25) * st;
    textID = RF_TextManager::Write(txt, {255,255,255}, p);

    if(-10 > pos.x){signal = S_KILL;}
}

void aleatLetra::Dispose()
{
  if("" < textID && RF_Engine::existsTask(textID))
  {
    RF_TextManager::DeleteText(textID);
  }
}

void aleatLetra::configure(string text, Vector2<int> StartPosition, int steps, int waitStep)
{
  txt=text;
  pos.x = (float)StartPosition.x;
  pos.y = (float)StartPosition.y;
  _steps = steps;
  sC = waitStep;
}
