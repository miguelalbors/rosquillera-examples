#include "../include/scene4.h"
#include <RosquilleraReforged/rf_assetmanager.h>

#include "../include/mainprocess.h"

void Scene4::Start(){
    RF_Engine::Debug(type);

    RF_Layer::Start();
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    SDL_Surface* bgImgS = RF_AssetManager::Get<RF_Gfx2D>("gfx", "euskal");//"eBaby");
    SDL_Surface* bgImgS2 = RF_AssetManager::Get<RF_Gfx2D>("gfx", "manos");//"logo");

    for(int i=0; i<640; i++)
    {
        for(int j=0; j<480; j++)
        {
            bgImg[i][j] = RF_Primitive::getPixel(bgImgS,i,j);
            bgImg2[i][j] = RF_Primitive::getPixel(bgImgS2,i,j);
            /*if((i >= 168 && j >= 50) && (i < 471 && j< 430))
            {
                bgImg[i][j] = RF_Primitive::getPixel(bgImgS,i-168,j-50);
            }
            else
            {
                bgImg[i][j] = 0xffffff;
            }*/
        }
    }
}

void Scene4::Update(){
    deltacount += RF_Engine::instance->Clock.deltaTime;
    if(0.025f < deltacount)
    {
        metang = M_PI * step * 0.075;

        /* Rotozoom 1 */
            crZ1 = cos(metang*0.025)*30;
            srZ1 = sin(metang*0.02)*30;

            sr2Z1=5+(sin(metang*0.2)*5)*0.5;

            xZ1 = crZ1 * cos(step*0.01)*100;
            yZ1 = srZ1 * sin(step*0.01)*100;

        /* Rotozoom 2 */
            crZ2 = cos(-metang*0.025)*30;
            srZ2 = sin(-metang*0.02)*30;

            sr2Z2=5+sin(-metang*0.1)*4.0;

            xZ2 = crZ2 * cos(step*0.01)*100;
            yZ2 = srZ2 * sin(step*0.01)*100;

        /* Metabolas */
            b1.x=300+cos(metang*1.25)*200; b1.y=300+sin(metang)*200;    b1.z=20;
            b2.x=300+sin(metang*0.9)*200;  b2.y=300+cos(metang)*200;    b2.z=25;
            b3.x=300+sin(metang+100)*200;  b3.y=300+cos(metang+50)*200; b3.z=30;

        for(int i = 0; i < 640; i++)
        {
            calci1 = pow(b1.x-i,2);
            calci2 = pow(b2.x-i,2);
            calci3 = pow(b3.x-i,2);

            calc12 = xZ2+abs((int)(i*sr2Z2));
            calc11 = xZ1+abs((int)(i*sr2Z1));
            calc32 = calc12+abs((int)(i*sr2Z2));

            for(int j = 0; j < 480; j++)
            {
                calcTemp = b1.z/(pow(sqrt(calci1+pow(b1.y-j,2)),goo[0])) + b2.z/(pow(sqrt(calci2+pow(b2.y-j,2)),goo[1])) + b3.z/(pow(sqrt(calci3+pow(b3.y-j,2)),goo[2]));

                if(calcTemp > 0.80)
                {
                    calc21 = yZ1+abs((int)(j*sr2Z1));

                    x=(calc11)*cos(step*0.01)-(calc21)*sin(step*0.01); while(x<0){x+=640;} while(x>639){x-=640;}
                    y=(calc11)*sin(step*0.01)+(calc21)*cos(step*0.01); while(y<0){y+=480;} while(y>479){y-=480;}

                    RF_Primitive::putPixel(graph,i,j,bgImg2[x][y]);
                }
                else if(calcTemp < 0.80 && calcTemp > 0.795)
                {
                    RF_Primitive::putPixel(graph,i,j,0x010101);
                }
                else
                {
                    calc22 = yZ2+abs((int)(j*sr2Z2));

                    x=(calc12)*(-cos(step*0.01))-(calc22)*sin(step*0.01); while(x<0){x+=640;} while(x>639){x-=640;}
                    y=(calc32)*(-sin(step*0.01))+(calc22)*cos(step*0.01); while(y<0){y+=480;} while(y>479){y-=480;}

                    RF_Primitive::putPixel(graph,i,j,bgImg[x][y]);
                }
            }
        }

        step++;
        deltacount = 0.0f;
    }

    if(16800 <= RF_Engine::instance->Clock.fixedCTime())
    {
        RF_Primitive::clearSurface(graph, 0x000000);
        RF_Engine::getTask<MainProcess>(father)->state() = 5;
        signal = RF_Structs::S_SLEEP;
    }
}
