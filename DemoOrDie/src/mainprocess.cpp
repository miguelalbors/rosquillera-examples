#include "../include/mainprocess.h"
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>

#include "../include/splashscreen.h"
#include "../include/scene1.h"
#include "../include/scene2.h"
#include "../include/scene3.h"
#include "../include/scene4.h"
#include "../include/scene5.h"
#include "../include/scene6.h"

void MainProcess::Start()
{
    RF_Engine::addWindow("Demo or die", 640, 480, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);

    RF_AssetManager::loadAssetPackage("res/gfx");
    RF_AssetManager::loadAssetPackage("res/misc");

    scene = RF_Engine::getTask(RF_Engine::newTask<SplashScreen>(id));
}

void MainProcess::Update()
{
  //ExeControl
    if(RF_Input::key[RF_Structs::_esc])
    {
      RF_Engine::Status() = false;
    }

    switch(stateMachine)
    {
      case 1: //Scene1
          if(scene && "Scene1" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_SoundManager::playSong("misc", "st7", 0);
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Scene1>(id));
          }
          break;
        case 2: //Scene2
          if(scene && "Scene2" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Scene2>(id));
          }
          break;
        case 3: //Scene3
          if(scene && "Scene3" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Scene3>(id));
          }
          break;
        case 4: //Scene4
          if(scene && "Scene4" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Scene4>(id));
          }
          break;
        case 5: //Scene5
          if(scene && "Scene5" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Scene5>(id));
          }
          break;
        case 6: //Scene6
          if(scene && "Scene6" != scene->type)
          {
            RF_Engine::sendSignal(scene, RF_Structs::S_KILL);
            scene = nullptr;
            RF_Engine::instance->Clock.setFixedCTime();

            scene = RF_Engine::getTask(RF_Engine::newTask<Scene6>(id));
          }
          break;
        case 7: //Fin
          RF_Engine::sendSignal(scene, RF_Structs::S_KILL_TREE);
          RF_Engine::Status() = false;

          stateMachine = -1;
          break;
      }
}
