#include "../include/scene6.h"
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_primitive.h>

#include "../include/mainprocess.h"
#include "../include/rf_3d.h"

void Scene6::Start()
{
    RF_Engine::Debug(type);

    RF_Layer::Start();
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    RF_TextManager::Font = RF_AssetManager::Get<RF_Font>("misc", "Times_New_Roman");

    RF_3D_Object* tmpObj = new RF_3D_Object();

    for(float i = -7.5; i < 7.5; i+=0.2)
    {
        for(float j = -7.5; j < 7.5; j+=0.2)
        {
            tmpObj->vertex.push_back(Vector3<float>(i,j,-1.3));
        }
    }

    tmpObj->finalizeCreation();
    tmpObj->transform.position = Vector3<float>(320.0f,240.0f,50.0f);
    tmpObj->transform.rotation = Vector3<float>(64.0f,60.0f,0.0f);
    tmpObj->transform.scale = Vector3<float>(50.0f,50.0f,50.0f);
    RF_3D::objectList.push_back(tmpObj);

    RF_3D::renderMode() = RM_LandScape;
}

void Scene6::Update()
{
    deltaCount += RF_Engine::instance->Clock.deltaTime;
    if(0.025f < deltaCount)
    {
        RF_Primitive::clearSurface(graph, 0x000000);

        for(int i = 0; i < RF_3D::objectList[0]->_vertex.size(); i++)
        {
            float x = RF_3D::objectList[0]->_vertex[i].x * RF_3D::objectList[0]->_vertex[i].y;
            RF_3D::objectList[0]->_vertex[i].z = -1.3 + cos((step+x)*0.05)*2.5;
        }

        RF_3D::Draw_Only(graph, 0);

        if(640 == step2)
        {
            switch(cuentatexto)
            {
                case 0:
                    bezierText("Thank's for watching my demo.",50);
                    bezierText("Programmed in C++ with ",100);
                    bezierText("La Rosquillera Framework.",150);

                    bezierText("Music by Stage7 - Genshiken ",300);
                    bezierText("Code by Mikel ",350);
                    step2 = 200;
                    break;

                case 1:
                    RF_Engine::sendSignalByType("BezierLetter", S_KILL);
                    step2 = 639;
                    break;

                case 2:
                    bezierText("El codigo completo se encuentra en ",100);
                    bezierText("https://yawin.es ",150);
                    bezierText("The scene will rock again ",300);
                    step2 = 200;
                    break;

                case 3:
                    RF_Engine::sendSignalByType("BezierLetter", S_KILL);
                    step2 = 639;
                    break;

                default:
                    bezierText("       M U C H A S   G R A C I A S           ", 190);
                    bezierText("      P O R   S U   A T E N C I O N          ", 240);
            }

            cuentatexto++;
        }
        step++;
        step2++;
        if(1327 < step2)
        {
          RF_Primitive::clearSurface(graph, 0x000000);
          RF_Engine::getTask<MainProcess>(father)->state() = 7;
          signal = RF_Structs::S_SLEEP;
        }

        deltaCount = 0.0f;
    }
}

void Scene6::bezierText(string text, int y){
    int x = 620;

    int emes = 0;
    for(int i = text.length()-1; i >= 0; i--)
    {
      if(text.substr(i,1) == "m")
      {
          emes++;
      }
    }
    for(int i = text.length()-1; i >= 0; i--)
    {
        if(text.substr(i,1) == " ")
        {
          x+=5;
        }
        else if(text.substr(i,1) == "m")
        {
          emes--;
        }

        RF_Engine::getTask<BezierLetter>(RF_Engine::newTask<BezierLetter>(id))->configure(text.substr(i,1),Vector2<int>(x+(3*emes),y),(text.length()-i)*10);
        x -= 17;
    }
}

void BezierLetter::Update()
{
    deltaCount+=RF_Engine::instance->Clock.deltaTime;
    if(0.025 < deltaCount && 0 <= step)
    {
        if("" < textID && RF_Engine::existsTask(textID))
        {
          RF_TextManager::DeleteText(textID);
        }

        if(100 >= step)
        {
            deltaCount = 0.0f;

            float beziert = step * 0.01;
            x = position.x*(1-beziert)*(pow(1-beziert,4)+9.5*pow(beziert,3));
            y = position.y*(pow(1-beziert,3)+pow(beziert,3))+(y-50)*3*(beziert-pow(beziert,2));
        }

        textID = RF_TextManager::Write(text, {255,255,255}, Vector2<int>(x, y));
        step--;
    }
}

void BezierLetter::Dispose()
{
  if("" < textID && RF_Engine::existsTask(textID))
  {
    RF_TextManager::DeleteText(textID);
  }
}
void BezierLetter::configure(string txt, Vector2<int> pos, int retardo)
{
  position = pos;
  text = txt;
  step+=retardo;
}
