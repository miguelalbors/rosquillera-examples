#include "../include/scene2.h"
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_primitive.h>

#include "../include/mainprocess.h"
#include "../include/rf_3d.h"

#include <math.h>
using namespace std;

void Scene2::Start()
{
    RF_Engine::Debug(type);

    RF_Layer::Start();
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    RF_TextManager::Font = RF_AssetManager::Get<RF_Font>("misc", "Times_New_Roman");

    RF_3D::loadObj("res/ico.yawobj");
    RF_3D::loadObj("res/cubo.yawobj");

    Transform3D t(Vector3<float>(320.0f,240.0f,240.0f),Vector3<float>(0.0f,0.0f,0.0f),Vector3<float>(50.0f,50.0f,50.0f));
    RF_3D::objectList[0]->transform = t;
    RF_3D::objectList[1]->transform = t;
}

void Scene2::Update()
{
    deltaCount += RF_Engine::instance->Clock.deltaTime;

    if(0.025f < deltaCount)
    {
        RF_Primitive::clearSurface(graph, 0x000000);
        step++; step2++;

        if(!lastFrame && cuentaobj == 0)
        {
            lastFrame = Starfield(16775);
        }

        if(16800 < RF_Engine::instance->Clock.fixedCTime())
        {
            if(cuentaobj>4)
            {
                lastFrame = Starfield(60000);
            }

            Transform3D tmpT = RF_3D::objectList[0]->transform;

            int tmpScl = cos(step*0.025) * 500;
            if(50 > tmpScl){tmpScl = 50;}

            if(50 == tmpScl)
            {
                RF_3D::renderMode() = RM_Mesh;
                cH = false;
            }
            else
            {
                RF_3D::renderMode() = RM_Circles;

                if(tmpScl > 400 && cH == false)
                {
                    cuentaobj++;
                    cH=true;
                    if(cuentaobj>2)
                    {
                        if(cuentaobj%2==0)
                        {
                            objtorend=0;
                        }
                        else
                        {
                            objtorend=1;
                        }
                    }
                }
            }

            tmpT.scale.x = tmpT.scale.y = tmpT.scale.z = tmpScl;
            tmpT.rotation.x += 0.05; tmpT.rotation.y += 0.055;//tmpT.scale.z = tmpScl;

            RF_3D::objectList[0]->transform = RF_3D::objectList[1]->transform = tmpT;
            RF_3D::Draw_Only(graph, objtorend);

            if(step2 > 100)
            {
                switch(textcont)
                {
                    case 0:
                        Scrolltext("Ada Lovelace", 100);
                        break;
                    case 1:
                        Scrolltext("Henrietta Swan Leavitt", 400);
                        break;
                    case 2:
                        stars.clear();
                        stars_speed.clear();
                        Scrolltext("Hedy Lamarr", 300);
                        break;
                    case 3:
                        Scrolltext("Jean Jennings Bartik", 400);
                        break;
                    case 4:
                        Scrolltext("Grace Cooper", 250);
                        break;
                    case 5:
                        Scrolltext("Margaret Hamilton", 150);
                        break;
                    case 6:
                        Scrolltext("Carol Shaw", 315);
                        break;
                    case 7:
                        Scrolltext("Barbara Liskov", 75);
                        break;
                    case 8:
                        Scrolltext("Xiaoyuan Tu", 350);
                        break;
                }
                textcont++;
                step2=0;
            }
        }

        deltaCount = 0.0f;
    }

    if(55078 <= RF_Engine::instance->Clock.fixedCTime())
    {
      RF_Primitive::clearSurface(graph, 0x000000);
      RF_Engine::getTask<MainProcess>(father)->state() = 3;
      signal = RF_Structs::S_SLEEP;
    }
    return;
}

Scene2::~Scene2(){
    RF_3D::objectList.clear();
}

bool Scene2::Starfield(int limit){
    if(100 > stars.size())
    {
        int xx = 320 + (rand()%100-50); if(320 == xx){xx = 315;}
        int yy = 240 + (rand()%100-50); if(240 == yy){yy = 235;}
        stars.push_back(Vector2<int>(xx,yy));
        stars_speed.push_back(15 + rand()%11);
    }

    bool painted = false;
    for(int i = 0; i < stars.size(); i++)
    {
        int xx = stars[i].x -( round(320 - stars[i].x) / stars_speed[i]);
        int yy = stars[i].y -( round(240 - stars[i].y) / stars_speed[i]);

        if(xx == stars[i].x && yy == stars[i].y)
        {
            xx = 320 + (rand()%100-50); if(320 == xx){xx = 315;}
            yy = 240 + (rand()%100-50); if(240 == yy){yy = 235;}
        }

        int dist = abs(xx - 320) + abs(yy - 240);
        int sCant;

        if(dist < 50)
        {
            sCant = 1;
        }
        else if(dist >= 50 && dist <= 100)
        {
            sCant = 2;
        }
        else if(dist >= 100 && dist <= 150)
        {
            sCant = 3;
        }
        else if(dist >= 150 && dist <= 200)
        {
            sCant = 4;
        }
        else if(dist > 200)
        {
            sCant = 5;
        }

        for(int i = 0; i < sCant; i++)
        {
            for(int j = 0; j < sCant; j++)
            {
                RF_Primitive::putPixel(graph,xx+i, yy+j, 0xffffff);
                painted = true;
            }
        }

        if(limit > RF_Engine::instance->Clock.fixedCTime())
        {
            if(0 > xx || 0 > yy || RF_Engine::MainWindow()->width() <= xx || RF_Engine::MainWindow()->height() <= yy)
            {
                xx = 320 + (rand()%100-50); if(320 == xx){xx = 315;}
                yy = 240 + (rand()%100-50); if(240 == yy){yy = 235;}
            }
        }

        stars[i].x = xx;
        stars[i].y = yy;
    }

    if(!painted)
    {
        stars.clear();
        stars_speed.clear();

        return true;
    }

    return false;
}
void Scene2::Scrolltext(string txt, int y)
{
    int posX = 643;
    float freq = 0.030 + (0.001 * (float)(5 - (1 + rand()%10)));

    int emes = 0;
    for(int i = txt.length()-1; i >= 0; i--)
    {
      if(txt.substr(i,1) == "m")
      {
          emes++;
      }
    }

    for(int i = 0; i < txt.length(); i++)
    {
        if(txt.substr(i,1) == " ")
        {
          posX+=5;
        }
        else if(txt.substr(i,1) == "m")
        {
          emes--;
        }

        RF_Engine::getTask<scrLetra>(RF_Engine::newTask<scrLetra>(id))->configure(txt.substr(i,1), Vector2<int>(posX+(3*emes),y), freq);
        posX+=20;
    }
}

void scrLetra::configure(string text, Vector2<int> StartPosition, float frecuencia)
{
    txt=text;
    pos.x = (float)StartPosition.x;
    pos.y = (float)StartPosition.y;

    freq = frecuencia;

    color = colores[rand()%7];
}

void scrLetra::Update()
{
    if("" < textID && RF_Engine::existsTask(textID))
    {
      RF_TextManager::DeleteText(textID);
    }

    int mod = 0;

    textID = RF_TextManager::Write(txt, color, Vector2<int>((int)pos.x, (int)(pos.y + cos((pos.x-mod)*freq)*30)));
    pos.x-=RF_Engine::instance->Clock.deltaTime*100;

    if(-10 > pos.x){signal = S_KILL;}
}

void scrLetra::Dispose()
{
  if("" < textID && RF_Engine::existsTask(textID))
  {
    RF_TextManager::DeleteText(textID);
  }
}
