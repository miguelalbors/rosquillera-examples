#include <RosquilleraReforged/rf_engine.h>

#include "include/mainprocess.h"

#include <unistd.h>
#include <iostream>
#include <string>
using namespace std;

int main(int argc, char *argv[])
{
	if(argc > 0)
	{
		string arg(argv[0]);
	  arg.erase(arg.find_last_of('/'), arg.size());
		chdir(arg.c_str());
	}

	RF_Engine::Start<MainProcess>(false);
	return 0;
}

#ifdef WIN
	int __stdcall WinMain(void*, void*, char*, int)
	{
		main(0, NULL);
		return 0;
	}
#endif
