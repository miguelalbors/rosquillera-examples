#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>

#include <unistd.h>
#include <iostream>
#include <string>
using namespace std;
using namespace RF_Structs;

class Imagen : public RF_Process
{
  public:
    Imagen():RF_Process("Imagen"){}
    virtual ~Imagen(){}

    virtual void Start()
    {
        RF_Engine::addWindow("Using assets", 480, 480);
        window = RF_Engine::MainWindow();
        transform.position.x = 240;
        transform.position.y = 240;

        RF_AssetManager::loadAssetPackage("snd");
        RF_AssetManager::loadAssetPackage("img");
    }

    int tecla = -1, old_tecla = -1;
    bool flanco = false;
    virtual void Update()
    {
      if(RF_Input::key[_esc])
      {
        RF_Engine::Status()=false;
      }

      if(RF_Input::key[_q]){tecla = 0;}
      else if(RF_Input::key[_w]){tecla = 1;}
      else if(RF_Input::key[_e]){tecla = 2;}
      else if(RF_Input::key[_r]){tecla = 3;}
      else if(RF_Input::key[_t]){tecla = 4;}
      else if(RF_Input::key[_y]){tecla = 5;}
      else if(RF_Input::key[_a]){tecla = 6;}
      else if(RF_Input::key[_s]){tecla = 7;}
      else if(RF_Input::key[_d]){tecla = 8;}
      else if(RF_Input::key[_f]){tecla = 9;}
      else if(RF_Input::key[_g]){tecla = 10;}

      if(RF_Input::key[_1]){ if(!flanco){flanco = true; RF_SoundManager::playFX("snd", "0");} }
      else if(RF_Input::key[_2]){ if(!flanco){flanco = true; RF_SoundManager::playFX("snd", "1");} }
      else if(RF_Input::key[_3]){ if(!flanco){flanco = true; RF_SoundManager::playFX("snd", "2");} }
      else if(RF_Input::key[_4]){ if(!flanco){flanco = true; RF_SoundManager::playFX("snd", "3");} }
      else if(RF_Input::key[_5]){ if(!flanco){flanco = true; RF_SoundManager::playFX("snd", "4");} }
      else{ flanco = false; }
    }

    virtual void Draw()
    {
      if(tecla != old_tecla)
      {
        old_tecla = tecla;
        graph = RF_AssetManager::Get<RF_Gfx2D>("img",to_string(tecla));
      }
    }
};

int main(int argc, char *argv[])
{
	if(argc > 0)
	{
		string arg(argv[0]);
	  arg.erase(arg.find_last_of('/'), arg.size());
		chdir(arg.c_str());
	}

	RF_Engine::Start<Imagen>(false);
	return 0;
}

#ifdef WIN
	int __stdcall WinMain(void*, void*, char*, int)
	{
		main(0, NULL);
		return 0;
	}
#endif
