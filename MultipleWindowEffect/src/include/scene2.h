#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_collision.h>
#include <RosquilleraReforged/rf_soundmanager.h>

#include <SDL2/SDL.h>

class Pelota : public RF_Process
{
	public:
		Pelota():RF_Process("Pelota"){}
		virtual void Start()
		{
			transform.position = Vector2<float>(300, 300);
			graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "r");
		}
};

class Pelota2 : public RF_Process
{
	public:
		Pelota2():RF_Process("Pelota2"){}
		virtual void Start()
		{
			transform.position = Vector2<float>(100, 300);
			graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "g");
			bolita = RF_Engine::getTask(father);
		}

		virtual void Update()
		{
			float tD = RF_Engine::instance->Clock.deltaTime*50;
			if(RF_Input::key[_a])
			{
				transform.position.x -= tD;
			}
			if(RF_Input::key[_d])
			{
				transform.position.x += tD;
			}
			if(RF_Input::key[_w])
			{
				transform.position.y -= tD;
			}
			if(RF_Input::key[_s])
			{
				transform.position.y += tD;
			}

			//RF_Engine::Debug(checkCollision(this, bolita));
			if(!RF_Collision::checkCollision(this, bolita))
			{
				//RF_SoundManager::changeMusic("common", "TestingTrack");
			}
			else
			{
				//RF_SoundManager::changeMusic("common", "CaravanPalace");
			}
		}

		RF_Process* bolita;
};

class Scene2 : public RF_Process
{
  public:
    Scene2():RF_Process("Scene2"){}
    ~Scene2()
		{
			RF_Engine::closeWindow(w);
		}

		int w;
    virtual void Start()
    {
      RF_AssetManager::loadAssetPackage("res/common_big");
	    RF_AssetManager::loadAssetPackage("res/common");

			w = RF_Engine::getWindow(RF_Engine::addWindow("Escena 2", 1024, 640));
			RF_Engine::MainWindow(w);
			RF_Engine::newTask<Pelota2>(RF_Engine::newTask<Pelota>(id));
    }

		string bola;
};
