#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_window.h>

class WindowMover : public RF_Process
{
	public:
		WindowMover():RF_Process("WindowMover"){}
		virtual ~WindowMover(){}

		virtual void Start()
		{
			deriv = 0.25;//((float)(10 + rand()%20))/50.0f;

			transform.position.x = window->x();
			transform.position.y = window->y();

			transform.scale.x = window->width();
			transform.scale.y = window->height();
		}
		virtual void Draw()
		{
			if(window!=nullptr)
			{
				window->move({transform.position.x + (int)(cos(3.14* p * deriv *2.5)*210), transform.position.y});
			}
			p+=RF_Engine::instance->Clock.deltaTime*2;
		}
	private:
		float deriv, deriv2;
		float p = 0;
};
