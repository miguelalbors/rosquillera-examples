#include "../include/plancha.h"
#include <RosquilleraReforged/rf_assetmanager.h>

#include "../include/mainprocess.h"

void Plancha::Start()
{
  RF_AssetManager::loadAssetPackage("./res/common");
  graph = RF_AssetManager::Get<RF_Gfx2D>("common", "plancha");
}

void Plancha::Update()
{
  if(!MainProcess::gameStarted){return;}
  tmp_spd = 0.0;

  if(RF_Input::key[_a])
  {
    tmp_spd = -1.0;
  }
  else if(RF_Input::key[_d])
  {
    tmp_spd = 1.0;
  }

  tmp_spd *= (speed*RF_Engine::instance->Clock.deltaTime);

  transform.position.x += tmp_spd;
  if(tmp_spd > 0.0 && transform.position.x > 600)
  {
    transform.position.x = 600;
  }
  else if(tmp_spd < 0.0 && transform.position.x < 40)
  {
    transform.position.x = 40;
  }
}
