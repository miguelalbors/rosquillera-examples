#ifndef PLANCHA_H
#define PLANCHA_H

#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

class Plancha : public RF_Process
{
  public:
    Plancha():RF_Process("Plancha"){}
    virtual ~Plancha(){}

    virtual void Start();
    virtual void Update();

  private:
    float speed = 500.0, tmp_spd = 0.0;
};

#endif //PLANCHA_H
