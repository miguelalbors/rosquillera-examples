#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

class MainProcess : public RF_Process
{
  public:
    MainProcess():RF_Process("MainProcess"){}
    virtual ~MainProcess(){}

    virtual void Start();
    virtual void Update();

    static bool gameStarted;

  private:
    void prepareGame();
    void preparePlayer();
    string pelota = "", plancha = "";
};

#endif //MAINPROCESS_H
