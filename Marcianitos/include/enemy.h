#ifndef ENEMY_H
#define ENEMY_H

#include <RosquilleraReforged/rf_process.h>
#include "../include/interfaces/alive.h"

class Enemy : public RF_Process, public Alive
{
  public:
    Enemy(string name = "Enemy"):RF_Process(name){}
    virtual ~Enemy(){}

    virtual void Update();
    virtual void OnAlive(){}
    virtual void OnDead(){}

  protected:
    RF_Structs::Vector2<float> speed;
};

class BasicEnemy : public Enemy
{
  public:
    BasicEnemy():Enemy(){}
    virtual ~BasicEnemy(){}

    virtual void Start();
    virtual void OnAlive();
    virtual void OnDead();

  private:
    int scale;
    string tipo;
};

#endif //ENEMY_H
