#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include <RosquilleraReforged/rf_process.h>

class MainProcess : public RF_Process
{
  public:
    MainProcess():RF_Process("MainProcess"){}
    virtual ~MainProcess(){}

    virtual void Start();
    virtual void Update();
};

#endif //MAINPROCESS_H
