#ifndef NAVE_H
#define NAVE_H

#include <RosquilleraReforged/rf_process.h>
#include "../include/interfaces/alive.h"

class Nave : public RF_Process, public Alive
{
  public:
    Nave():RF_Process("Nave"){}
    virtual ~Nave(){}

    virtual void Start();
    virtual void Update();

  private:
    float speed = 800.0;
    RF_Structs::Vector2<float> limites[2] = {{0, 640},{240,480}};
    float shooting = 0.0, shooting_speed = 4.0;

    void resetNave();
    float invulnerable;
};

#endif //NAVE_H
