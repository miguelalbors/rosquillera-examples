#include "../include/enemy.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_engine.h>

void Enemy::Update()
{
  if(isAlive())
  {
    if( transform.position.x + graph->w < 0.0   ||
        transform.position.x - graph->w > 640.0 ||
        transform.position.y - graph->h > 480.0
    )
    {
      signal = RF_Structs::S_KILL;
    }

    OnAlive();
  }
  else
  {
    OnDead();
    graph = explosion();
    if(graph == nullptr)
    {
      signal = RF_Structs::S_KILL;
    }
  }
}

void BasicEnemy::Start()
{
  scale = rand()%4;
  setLive(1);

  RF_AssetManager::loadAssetPackage("./res/enemies");

  switch(scale)
  {
    case 0:
      tipo = "enemigo";
      break;
    case 1:
      tipo = "enemigo2";
      break;
    case 2:
      tipo = "enemigo3";
      break;
    case 3:
      tipo = "enemigo4";
      break;
  }

  graph = RF_AssetManager::Get<RF_Gfx2D>("enemies", tipo);

  transform.position = {20 + rand()%600, -40};
  speed = {(float)(-100 + rand()%200), (float)(200 + rand()%100)};

  zLayer = 10;
}

void BasicEnemy::OnAlive()
{
  transform.position.x += speed.x * RF_Engine::instance->Clock.deltaTime;
  transform.position.y += speed.y * RF_Engine::instance->Clock.deltaTime;
}

void BasicEnemy::OnDead()
{
  OnAlive();
}
