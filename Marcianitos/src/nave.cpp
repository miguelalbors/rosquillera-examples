#include "../include/nave.h"
#include "../include/enemy.h"
#include "../include/laser.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_collision.h>

using namespace RF_Structs;

void Nave::Start()
{
  RF_AssetManager::loadAssetPackage("./res/common");
  zLayer = 1;
  resetNave();
}

void Nave::resetNave()
{
  graph = RF_AssetManager::Get<RF_Gfx2D>("common", "nave");
  transform.position = {320,400};
  invulnerable = 2.0;
  isDestructible = false;
  expl = 1.0;
  setLive(3);
}

void Nave::Update()
{
  if(invulnerable > 0.0)
  {
    invulnerable -= RF_Engine::instance->Clock.deltaTime;
  }
  else if(isDestructible == false)
  {
    isDestructible = true;
  }

  if(!isAlive())
  {
    graph = explosion();
    if(graph == nullptr)
    {
      resetNave();
    }
  }
  else
  {
    if(RF_Input::key[_space])
    {
      shooting -= RF_Engine::instance->Clock.deltaTime * shooting_speed;
      if(shooting <= 0.0)
      {
        shooting = 1.0;
        Laser::Shoot({transform.position.x, transform.position.y - graph->h}, {0.0, -200.0}, id);
      }
    }
    else
    {
      shooting = 0.0;
    }

    if(RF_Input::key[_a] && !RF_Input::key[_d])
    {
      transform.position.x -= speed * RF_Engine::instance->Clock.deltaTime;
    }
    else if(!RF_Input::key[_a] && RF_Input::key[_d])
    {
      transform.position.x += speed * RF_Engine::instance->Clock.deltaTime;
    }

    if(transform.position.x - (graph->w >> 1) < limites[0].x)
    {
      transform.position.x = limites[0].x + (graph->w >> 1);
    }
    else if(transform.position.x + (graph->w >> 1) > limites[0].y)
    {
      transform.position.x = limites[0].y - (graph->w >> 1);
    }

    if(RF_Input::key[_w] && !RF_Input::key[_s])
    {
      transform.position.y -= speed * RF_Engine::instance->Clock.deltaTime;
    }
    else if(!RF_Input::key[_w] && RF_Input::key[_s])
    {
      transform.position.y += speed * RF_Engine::instance->Clock.deltaTime;
    }

    if(transform.position.y - (graph->h >> 1) < limites[1].x)
    {
      transform.position.y = limites[1].x + (graph->h >> 1);
    }
    else if(transform.position.y + (graph->h >> 1) > limites[1].y)
    {
      transform.position.y = limites[1].y - (graph->h >> 1);
    }

    Enemy *collided = reinterpret_cast<Enemy*>(RF_Collision::checkCollisionByType(this, "Enemy"));
    if(collided && collided->getHP() > 0)
    {
      collided->doDamage(1);
      doDamage(1);
    }
  }
}
