#include "../include/background.h"
#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_primitive.h>
#include <RosquilleraReforged/rf_assetmanager.h>

void Background::Start()
{
  RF_Layer::Start();
  RF_AssetManager::loadAssetPackage("./res/common");
  fondo = RF_AssetManager::Get<RF_Gfx2D>("common", "fondo");

  transform.position = {320, 240};
}

void Background::Update()
{
  offset -= RF_Engine::instance->Clock.deltaTime*100;
  if(offset < -(fondo->h)){offset = 0.0;}
}

void Background::Draw()
{
  ioffset = (int)offset;

  for(int j = 0; j < graph->h; j++)
  {
    for(int i = 0; i < graph->w; i++)
    {
      tmp_j = (j+ioffset)%fondo->h;
      if(tmp_j < 0){tmp_j = fondo->h + tmp_j;}

      RF_Primitive::putPixel(graph, i, j, RF_Primitive::getPixel(fondo, i, tmp_j));
    }
  }
}
